# hook: pre-commit
pre-commit:
	true
.PHONY: pre-commit

# hook: prepare-commit-msg
prepare-commit-msg:
	true
.PHONY: prepare-commit-msg

# hook: commit-msg
commit-msg:
	true
.PHONY: commit-msg

# hook: post-commit
post-commit:
	true
.PHONY: post-commit

# hook: pre-rebase
pre-rebase:
	true
.PHONY: pre-rebase

# hook: post-rewrite
post-rewrite:
	true
.PHONY: post-rewrite

# hook: post-merge
post-merge:
	true
.PHONY: post-merge

# hook: pre-push
pre-push:
	true
.PHONY: pre-push
