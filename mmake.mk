# An example stanza to include in a base Makefile to install mmake

# display usage
mmake:
	mmake help
.PHONY: mmake

# install modern make
install-mmake:
	go get -u github.com/tj/mmake/cmd/mmake
.PHONY: install-mmake
