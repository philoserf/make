# display usage
mmake:
	mmake help
.PHONY: mmake

# install modern make
install-mmake:
	go get -u github.com/tj/mmake/cmd/mmake
.PHONY: install-mmake

include github.com/philoserf/make/githooks
include github.com/philoserf/make/cicd
include github.com/philoserf/make/mdlint
