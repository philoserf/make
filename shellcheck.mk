# install shallcheck
install-shallcheck:
	brew install shellcheck
.PHONY: install-shallcheck

shell_files := $(shell git ls-files|grep -e \\.sh$)

# check shell files for lint, format, and errors
shellcheck:
	shellcheck $(shell_files)
.PHONY: shellcheck
