# ci: lint check
lint:
	true
.PHONY: lint

# ci: syntax check
syntax:
	true
.PHONY: syntax

# ci: unit test
unit:
	true
.PHONY: unit

# ci: build
build:
	true
.PHONY: build

# ci: publish build artifact
publish:
	true
.PHONY: publish

# ---------------------------------------------------------------------

# ci: provision environment
provision:
	true
.PHONY: provision

# ci: deploy to provisioned environment
deploy:
	true
.PHONY: deploy

# ci: smoke test
smoke:
	true
.PHONY: smoke

# ci: functional test
functional:
	true
.PHONY: functional

# ci: e2e test
e2e:
	true
.PHONY: e2e

# ci: performance test
performance:
	true
.PHONY: performance

# ---------------------------------------------------------------------

# ci: clean
clean:
	true
.PHONY: clean
