# install shfmt
install-shfmt:
	go get -u mvdan.cc/sh/cmd/shfmt
.PHONY: install-shfmt

shell_files := $(shell git ls-files|grep -e \\.sh$)

# check shell files for format issues
shfmt:
	shfmt $(shell_files)
.PHONY: shfmt
