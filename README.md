# Make

My include files for [modern make][1]

- lint and format
  - [mdlint.mk][3]
  - [ymllint.mk][4]
  - [shellcheck.mk][5]
  - [shfmt.mk][6]
- example
  - [mmake.mk][7]
- golang
  - [gometalinter][8]

---

Copyright 2018 by Mark Ayers. License: [Apache 2.0][2]

[1]: https://github.com/tj/mmake
[2]: LICENSE.md
[3]: mdlint.mk
[4]: ymllint.mk
[5]: shellcheck.mk
[6]: shfmt.mk
[7]: mmake.mk
[8]: gometalinter.mk
