# install gometalinter
install-gometalinter:
	go get -u github.com/alecthomas/gometalinter
	gometalinter --install
.PHONY: install-gometalinter

# gometalinter
gometalinter:
	gometalinter \
		--vendored-linters \
		--disable-all \
		--enable=vet \
		--enable=gofmt \
		--enable=golint \
		--sort=path \
		--aggregate \
		--vendor \
		--tests \
		./...
.PHONY: gometalinter
