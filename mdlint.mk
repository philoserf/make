# install markdownlint
install-markdownlint:
	npm -g install markdownlint-cli
.PHONY: install-markdownlint

md_files := $(shell git ls-files|grep -e \\.md$)

# check markdown files for lint, format, and errors
mdlint:
	markdownlint $(md_files)
.PHONY: mdlint
